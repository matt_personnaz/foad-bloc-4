# sq.py
# Stack and queue classes

# Implement a Stack by taking a list as composite
class Stack:
    def __init__(self):
        self.list = []
 		
	# add an item to the top of stack
    def push(self, item):
        self.list.append(item)
		
	# remove an item from the top of the stack
    def pop(self):
        return self.list.pop()
		
	# read the top of the stack
    def peek(self):
        return self.list[0]
		
	# return True if the stack is empty
    def is_empty(self):
        return not self.list  
    
    def clear(self):
        self.list = []
        

 # Implement a Queue by taking a list as composite
class Queue:
    def __init__(self):
        self.list = []
    
    #add an item to back of the queue
    def enqueue(self, item):
            self.list.append(item)
   
    
    # remove an item from the front of the queue
    def dequeue(self):
    		temp = self.list[0]
    		del self.list[0]
    		return temp
		
 	# return True if the queue is empty
    def is_empty(self):
    		return not self.list
    		
    def clear(self):
    		self.list = []