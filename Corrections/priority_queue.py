# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 11:59:05 2020

@author: Matt
"""

# Node of a chained list
class Node:
    
    def __init__(self, data=None, priority=0):
        self.__data = data
        self.__priority = priority
        self.__ne = None
    
        
    def __repr__(self):
        return str(self.__data)
    
    def set_next(self, next):
        self.__next = next
    
    def get_next(self):
        return self.__next
    
    def set_data(self, data):
        self.__data = data
   
    def get_data(self):
        return self.__data 
    
    def get_priority(self):
        return self.__priority
    
        

# Implements a priority queue
class PriorityQueue:
    def __init__(self):
        self.__head = None
        
    def __repr__(self):
        cur = self.__head
        nodes = []
        while cur is not None:
            nodes.append(str(cur.get_data()))
            cur = cur.get_next()
        nodes.append("None")
        return "<-".join(nodes)
        
    
     
    #Return the value at head  
    def peek(self):
        return self.__head
    

    # Remove node from the front of the queue
    def dequeue(self):
        assert self.__head is not None, "The list is empty"
        self.__head = self.__head.get_next()
        
    # to enqueue to the back of the queue according to priority  
    def enqueue(self, node):
        if self.__head is None:
            self.__head = node
            node.set_next(None)
            
        elif node.get_priority() > self.__head.get_priority():
            node.set_next(self.__head)
            self.__head = node
        else: # either at the end of the list or at the required position
            cur = self.__head
            while cur.get_next() is not None and cur.get_next().get_priority() >= node.get_priority():
                cur = cur.get_next()
            node.set_next(cur.get_next())
            cur.set_next(node)
        
    # Return True if the queue is empty
    def is_empty(self):
       return not self.__head
    
               
    def clear(self):
        self.__head = None
            
    
    # Return the size of the queue
    def size(self):
        count = 0
        cur = self.__head
        while cur is not None:
            cur = cur.get_next()
            count = count + 1
        return count
    
   
      
        
if __name__=="__main__":
    c1 = Node("jeune Isa", 2)
    c2 = Node("jeune Rayane", 2)
    c3 = Node("ancien Léon", 5)
    c4 = Node("urgentiste Delphine", 3)
    ll = PriorityQueue()

    ll.enqueue(c1)
    ll.enqueue(c2)
    ll.enqueue(c3)
    ll.enqueue(c4)



    
   
        

    
    
        