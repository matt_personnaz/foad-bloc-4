# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 11:59:05 2020

@author: Matt
"""


# Node of a chained list
class Node:   
    def __init__(self, data):
        self.__data = data
        self.__ne = None
    
        
    def __repr__(self):
        return str(self.__data)
    
    def set_ne(self, ne):
        self.__ne = ne
    
    def get_ne(self):
        return self.__ne
    
    def set_data(self, data):
        self.__data = data
   
    def get_data(self):
        return self.__data 


#Clist implement a chained list
class Clist:
    def __init__(self):
        self.__head = None #Init the head of the list to None
        
    def __repr__(self):
        cur = self.__head
        nodes = []
        while cur is not None:
            nodes.append(str(cur.get_data()))
            cur = cur.get_ne()
        nodes.append("None")
        return "->".join(nodes)
        
    
    def set_head(self, head):
        self.__head = head
        
    def get_head(self):
        return self.__head     
    
    # add the given node to the head of the list
    def add_head(self, node):
        node.set_ne(self.__head)
        self.__head = node
    
    # add the given note to the queue of the list
    def add_queue(self, node):
        cur = self.__head
        while cur.get_ne() is not None:
            cur = cur.get_ne()
        cur.set_ne(node)
        node.set_ne(None)
    
    # return true if the list is empty
    def is_empty(self):
       return not self.__head
    
               
    # empty the list
    def clear(self):
        self.__head = None
            
    
    # return the size of the list
    def size(self):
        count = 0
        cur = self.__head
        while cur is not None:
            cur = cur.get_next()
            count = count + 1
        return count
    
    # get the node at the ii position in the list
    def get(self, ii):
        if self.is_empty():
            raise Exception("the list is empty")
      
        cur = self.__head
        for kk in range(ii):
            if cur.get_ne() is None:
                raise Exception("size of list is less than ", ii + 1)
            cur = cur.get_ne()
        return cur
    
    # remove the last node of the list
    def pop(self):
        cur = self.__head
        if self.__head is None:
           raise Exception("the list is empty")
           return
       
        if cur.get_ne() is None:
            self.__head = None
            return
        if cur.get_ne().get_ne() is None:
            cur.set_ne(None)
            return
       
        while cur.get_ne().get_ne() is not None:
            cur = cur.get_ne()
            if cur.get_ne().get_ne() is None:
                cur.set_ne(None)
                break
            
    
    # return true if the list contains the node
    def find(self, node):
        ref = node.get_data()
        print(ref)
        found = False
        cur = self.__head
        while cur is not None:
            temp = cur.get_data()
            if temp == ref:
                found = True
                break
            cur = cur.get_ne()
        return found
    
    
    # insert the given node at the given position pos
    def insert(self, pos, node):
        cur = self.__head
        if pos == 0:
            node.set_ne(self.__head)
            self.__head = node
            return
        for ii in range(pos - 1):
            cur = cur.get_ne()
        print(cur.get_ne())
        node.set_ne(cur.get_ne())
        cur.set_ne(node)
        
      
        
if __name__=="__main__":
    c1 = Node(2)
    c2 = Node(43)
    c3 = Node(25)
    list1 = Clist()
    list1.add_head(c1)
    list1.add_head(c2)
    list1.add_head(c3)
    ll = Clist()
    tab = [43, 34, 21]
   
    
   
        

    
    
        