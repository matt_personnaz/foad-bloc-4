# -*- coding: utf-8 -*-
"""
Created on Fri Jun 08 09:48:54 2020

@author: Alexandre Dicharry
"""

#########################################################################################################################################################
###ACTIVITE 1 - ANALYSE ET RECHERCHE ####################################################################################################################
#########################################################################################################################################################

##A FAIRE Q1 ##

def inverse_word(ch):
    assert type(ch) == str, "Vous devez rentrer une chaîne de caractères" #assertion sur le type de l'argument
    envers = ""
    for element in ch : #boucle for qui nous permet de recomposer le mot à l'envers
        envers = element + envers
    return envers
    
    
##Pour vérifier, activez les lignes ci-dessous :

#print("amuser s'écrit à l'envers ",inverse_word("amuser"),". On dit que c'est un mot anacyclique.")
#print("ressasser s'écrit de la même façon à l'envers ",inverse_word("ressasser")," C'est un cas particulier d'anacyclique dit palindrome")

#########################################################################################################################################################
##Ci-dessous, la classe Pile que vous utiliserez par la suite. ##########################################################################################
#########################################################################################################################################################

class Stack:
    """Classe Pile"""
    
    def __init__(self):
        """Constructeur de la classe"""
        self.__stack = []
    
    def get_stack(self):
        """Afficheur"""
        s = 'Pile : '
        for e in self.__stack :
            s = s + str(e) + ' '
        return s
    
    def push(self,x):
        """Empile un élément x au sommet de la pile"""
        self.__stack.append(x)
    
    def pop(self):
        """Depile le sommet de la pile et le retourne"""
        return self.__stack.pop()
    
    def top_peek(self):
        """Retourne le sommet de la pile"""
        try :
            return self.__stack[-1]
        except IndexError:
            print("La pile est vide")
    
    def is_empty(self):
        """Test si la pile est vide (retourne True) ou non (retourne False)"""
        return len(self.__stack) == 0

########################################################################################################################################################
###ACTIVITE 2 - APPLICATIONS############################################################################################################################
########################################################################################################################################################

##A FAIRE Q3 à Q5 ##

def bien_parenthese(ch):
    bp = True
    P = Stack() #utilisation de la classe Stack
    i = 0
    while bp and i < len(ch) :
        if ch[i] == "(" :
            P.push(ch[i]) #méthode pusch()
        elif ch[i] == ")":
            if P.top_peek() != "(" : #méthode top_peek()
                bp = False
            else :
                P.pop() #méthode pop()
        i = i+1        
    if P.is_empty() == False : #méthode is_empty()
         bp = False
    return bp


##Pour vérifier, activez les lignes ci-dessous :

#print("Activité 2 - Q4")
#ch1 = "(5x3)+7"
#ch2 = "(5x3)+6)"
#print(ch1," est bien parenthésée : ",bien_parenthese(ch1))
#print(ch2," est bien parenthésée : ",bien_parenthese(ch2))

##A FAIRE Q5##

def bien_parenthesebis(ch):
    bp = True
    P = Stack() 
    i = 0
    while bp and i < len(txt):
        if ch[i] == "("  or  ch[i] == "[":
            P.push(ch[i])
        elif ch[i] == ")":
            if P.top_peek() != "(":
                bp = False
            else:
                P.pop()
        elif ch[i] == "]":
            if P.top_peek() != "[":
                bp = False
            else:
                P.push()
        i = i+1
    if P.is_empty() == False :
        bp = False
    return bp

##Pour vérifier, activez les lignes ci-dessous :

#print("Activité 2 - Q5")
#ch1 = "(5x3)+[7x6]"
#ch2 = "(5x3)+[6x9)"
#print(ch1," est bien parenthésée : ",bien_parenthese(ch1))
#print(ch2," est bien parenthésée : ",bien_parenthese(ch2))


##A FAIRE Q6 et Q8##


def stack(n):#création d'une pile de longueur n
    Pile = [None]*(n+1) #on crée une case de plus qui contiendra l'indice de la prochaine case à remplir
    Pile[0] = 1 #on initialise la première case à 1
    return Pile    

def push(P,x) : #on insère la donnée x au sommet de la pile si celle-ci n'est pas pleine
    if P[0] == len(P) :
        print("La pile est pleine")
        return False
    else :
        i = P[0]
        P[i] = x #on place la donnée x au sommet de la pile
        P[0] = P[0] + 1 #on augmente de 1 l'indice de la prochaine case à remplir
        return True

def pop(P) : #on retire la dernière donnée de la pile si elle n'est pas vide et on la retourne
    if P[0] == 1 :
        print("La pile est vide")
        return False
    else :
        i = P[0] #on récupére l'indice de la prochaine case vide
        j = P[i-1] #la donnée au sommet est stockée en j
        P[i-1] = None #on retire la donnée qui est au sommet
        P[0] = P[0] - 1 #on diminue de 1 l'indice d ela prochaine case vide
        return j #on retourne la donnée qui était initialement au sommet

def is_full(P) : #on teste si la pile est pleine ou pas
    return P[0] == len(P)

def is_empty(P) : #on teste si la pile est vide ou pas
    return P[0] == 1

def top_peek(P) : #on retourne sans l'enlever la donnée en haut de la pile
    if P[0] == 1 :
        print("La pile est vide")
        return False
    else :
        i = P[0] #on récupére l'indice de la prochaine case vide
        return P[i-1] #la donnée au sommet est retournée def Empiler(P,x):

##Pour vérifier, activez les lignes ci-dessous :
    
#P = stack(4)
#print(P)
#push(P,3)
#push(P,5)
#push(P,8)
#N=pop(P)
#push(P,11)
#push(P,13)
#print("Pile P :",P,", N = ",N)
#print("is_empty(P) renvoie ",is_empty(P))
#print("is_full(P) renvoie ",is_full(P))
#print("top_peek() donne ",top_peek(P))


#A FAIRE Q10##

def inverse_stack(P): #permet d'inverser une pile
    PP = Stack()
    for i in range(len(P)):
        PP.push(P.pop())
    return PP

def calculatrice_polonaise(P):
    PP = inverse_stack(P)
    resultat_trouve = False
    resultat = 0
    while resultat_trouve == False :
        A = PP.pop() #méthode pop()
        if PP.is_empty() == True : #méthode is_empty()
            resultat_trouve = True
            resultat = A
            return resultat
        else :
            B = PP.pop() #méthode pop()
            op = PP.pop() #idem
            if op == '+' :
                PP.push(A+B) #méyhode pusch()
            else :
                if op == '-' :
                    PP.push(A-B) #idem
                else :
                    if op == '*':
                        PP.push(A*B) #idem
                    else :
                        PP.push(A/B) #idem

#Pour vérifier, activez les lignes ci-dessous :

#P = [7,2,"+",24,"*"]
#print("24*(7+2) = ",calculatrice_polonaise(P))
                        
########################################################################################################################################################
########################################################################################################################################################
########################################################################################################################################################

