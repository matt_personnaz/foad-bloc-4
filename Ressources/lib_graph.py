# -*- coding: utf-8 -*-
"""
Created on Sat Jun 27 07:45:48 2020

@author: Matt
"""


""" A Python Class for Non Oriented Graphs
"""

from graphviz import Digraph


class Error(Exception):
    """Base class for exceptions in this module."""
    pass


class GraphError(Error):
    """Exception raised for self loops
    """
    def __init__(self, message):
        self.message = message


class Graph(object):
    """ Class for non orientied graphs"""
    def __init__(self, graph_dict=None):
        """ initializes a graph object
            If no dictionary or None is given,
            an empty dictionary will be used
        """
        if graph_dict is None:
            graph_dict = {}
        self.__graph_dict = graph_dict

    
    def adjacency_list(self):
        """ print the adjency list """
        print(self.__graph_dict)
        
    def vertices(self):
        """ returns the vertices of a graph """
        return list(self.__graph_dict.keys())

    def edges(self):
        """ returns the edges of a graph """
        return self.__generate_edges()

    def neighbours(self, vertex):
        """ returns the neighbours of a given vertex"""
        return self.__graph_dict[vertex]

    def add_vertex(self, vertex):
        """ ff the vertex "vertex" is not in
            self.__graph_dict, a key "vertex" with an empty
            list as a value is added to the dictionary.
            Otherwise nothing has to be done.
        """
        if vertex not in self.__graph_dict:
            self.__graph_dict[vertex] = []

    def add_edge(self, edge):
        """ add an edge. Edge should be a pair and not (c,c)
        """ 
        try:
            (vertex1, vertex2) = edge
            if vertex1 == vertex2:
                raise GraphError("no self loop")
            if vertex1 in self.__graph_dict:
                if vertex2 not in self.__graph_dict[vertex1]:
                    self.__graph_dict[vertex1].append(vertex2)
            else:
                self.__graph_dict[vertex1] = [vertex2]
            if vertex2 in self.__graph_dict:
                 if vertex1 not in self.__graph_dict[vertex2]:
                    self.__graph_dict[vertex2].append(vertex1)
            else:
                self.__graph_dict[vertex2] = [vertex1]
        except GraphError as s:
            print("pb with adding edge: "+s.message)
            pass

    def __generate_edges(self):
        """ a static method generating the set of edges
        (they appear twice in the dictionnary). Returns a list of sets.
        """
        edges = []
        for vertex in self.__graph_dict:
            for neighbour in self.__graph_dict[vertex]:
                if {neighbour, vertex} not in edges:
                    edges.append({vertex, neighbour})
        return edges

    def __str__(self):
        res = "vertices: "
        for k in self.__graph_dict:
            res += str(k) + " "
        res += "\nedges: "
        for edge in self.__generate_edges():
            res += str(edge) + " "
        return res


    def delete_vertex(self, vertex): 
        """ delete vertex and all the adjacent edges"""
        gdict = self.__graph_dict
        if vertex in gdict:
            for neighbour in gdict[vertex]:
                if neighbour in gdict:
                    gdict[neighbour].remove(vertex)
            del gdict[vertex]

    def delete_edge(self, edge):
        """ delete edge"""
        (v1, v2) = edge
        self.__graph_dict[v1].remove(v2)
        self.__graph_dict[v2].remove(v1)

    def dfs_traversal(self, root): 
        pass

    def bfs_traversal(self, root):
        pass
                
    def detect_cycle(self, vv):
        """ Returns true if the graph contains a cycle using DFS 
        """

        st = [vv] # stack
        seen = []
        gdict = self.__graph_dict
        while len(st) > 0:
            cur = st.pop() #pull cur from stack
            for neighbour in gdict[cur]:
                if neighbour not in seen:
                    st.append(neighbour)
            
            if cur in seen:
                return True
            else:
                seen.append(cur)
        return False
    
    
    def __detect_cycle_rec(self,vv,seen,parent): 
        """ a recursive function which uses visited[] and parent 
        to detect cycle in subgraph reachable from vertex v """"""
        """
        seen.append(vv)
        
  
        #Recur for all the vertices adjacent to this vertex 
        for ii in self.__graph_dict[vv]: 
            # If the node is not visited then recurse on it 
            if  ii not in seen:  
                if(self.__detect_cycle_rec(ii,seen,vv)): 
                    return True
            # If an adjacent vertex is visited and not parent of current vertex, 
            # then there is a cycle 
            elif  parent!=ii: 
                return True
          
        return False
           
   
    def detect_cycle_rec(self): 
        """ recursive function to detect cycle using DFS"""
        
        seen = []
        
        for cur in self.vertices(): 
            if cur not in seen: #Don't recur for u if it is already visited 
                if(self.__detect_cycle_rec(cur,seen,-1))== True: 
                    return True
        return False
    
    def is_reachable_from(self, v1, v2):
        # Could be better implemented
        return v2 in self.dfs_traversal(v1)

    def connex_components(self):
        components = []
        todo = self.vertices()
        done = []
        # TODO : find CCs 
        # BEGIN CUT
        while todo:
            v = todo.pop()
            if v not in done:
                v_comp = self.dfs_traversal(v)
                components.append(v_comp)
                done.extend(v_comp)
        # END CUT
        return components


    # One non optimal coloring heuristic based on Kempe's proposition
    # returns False if the graph is not colorable with this heuristic
    def color(self, K):  # color with <= K color, returns a map vertex-> color
        todo_vertices = []
        gcopy = deepcopy(self)
        while gcopy.__graph_dict:
            todo = list(gcopy.__graph_dict)
            todo.sort(key=lambda v: len(gcopy.__graph_dict[v]))
            lower = todo[0]
            todo_vertices.append(lower)
            gcopy.delete_vertex(lower)
        # print todo_vertices
        coloring = {}
        gdict = self.__graph_dict
        for v in todo_vertices:
            seen_neighbours = [x for x in gdict[v] if x in coloring]
            choose_among = [i for i in range(K)
                            if not(i in [coloring[v1]
                                         for v1 in seen_neighbours])]
            if choose_among:
                color = min(choose_among)
                coloring[v] = color
            else:
                return False
        return coloring
    

    def print_dot(self, name="graph", colors={}):
        """
        Use graphviz to print the graph. Colors is optional.
        """
        foo = ['red', 'blue', 'green', 'yellow'] + (['black'] * 10)
        dot = Digraph(comment='My Graph')
        for k in self.__graph_dict:
            if not(colors):
                dot.node(k, k, color="red")
            else:
                dot.node(k, k, color=foo[colors[k]])
        for edge in self.__generate_edges():
            # print(edge)
            (v1, v2) = list(edge)[0], list(edge)[1]
            dot.edge(v1, v2, dir="none")
        # print(dot.source)
        dot.render(name, view=True)        # print in pdf
        
if __name__ == '__main__':
    pass