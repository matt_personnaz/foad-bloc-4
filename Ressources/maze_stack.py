# This program traverses a maze using a stack.

from sq import Stack              # import a Stack type from sq.py

MAZE_SIZE = 10                    # define the size of the maze
		
# print a maze
def print_maze(maze):
	print("-------------------------")	
	for row in range(MAZE_SIZE):
		print(maze[row])
	print("-------------------------")	


# Return true if the given (x,y) point is in the maze
def in_bounds(xx,yy):              
	return (0 <= xx < MAZE_SIZE) and (0 <= yy < MAZE_SIZE)
	
# traverse 'maze' from starting coordinates 'start'
def solve_maze(maze, start):          
	st = Stack()                  
	st.push(start);               # push the start coordinates onto s
	while not st.is_empty():      # loop while stack is not empty
		(xx, yy) = st.pop()          # pop a coordinate off s into the tuple (x,y)
		if in_bounds(xx,yy):         
			if maze[xx][yy] == 'G':    # test if (x,y) is the goal 
				st.clear()             # empty the stack because we're done
			elif maze[xx][yy] == ' ':  # else if (x,y) is an undiscovered coordinate
				maze[xx] = maze[xx][:yy] + 'D' + maze[xx][yy+1:]  # mark (x,y) discovered with 'D'
				print_maze(maze);      # print the maze to show progress so far
				# to complete     
				
if __name__=="__main__":
    input = open('maze.dat', 'r')     
    maze = input.readlines();         # read the file into maze
    solve_maze(maze, (0,0))           # traverse the maze starting at (0,0)
