# -*- coding: utf-8 -*-
"""
Created on Fri Jun 08 09:48:54 2020

@author: Alexandre Dicharry
"""

#########################################################################################################################################################
###ACTIVITE 1 - ANALYSE ET RECHERCHE ####################################################################################################################
#########################################################################################################################################################

##A FAIRE Q1 ##

def inverse_word(ch):
    pass    
    
    
    
    
    
    
##Pour vérifier, activez les lignes ci-dessous :

#print("amuser s'écrit à l'envers ",inverse_word("amuser"),". On dit que c'est un mot anacyclique.")
#print("ressasser s'écrit de la même façon à l'envers ",inverse_word("ressasser")," C'est un cas particulier d'anacyclique dit palindrome")

#########################################################################################################################################################
##Ci-dessous, la classe Pile que vous utiliserez par la suite. ##########################################################################################
#########################################################################################################################################################

class Stack:
    """Classe Pile"""
    
    def __init__(self):
        """Constructeur de la classe"""
        self.__stack = []
    
    def get_stack(self):
        s = 'Pile : '
        for e in self.__stack :
            s = s + str(e) + ' '
        return s
    
    def push(self,x):
        """Empile un élément x au sommet de la pile"""
        self.__stack.append(x)
    
    def pop(self):
        """Depile le sommet de la pile et le retourne"""
        return self.__stack.pop()
    
    def top_peek(self):
        """Retourne le sommet de la pile"""
        try :
            return self.__stack[-1]
        except IndexError:
            print("La pile est vide")
    
    def is_empty(self):
        """Test si la pile est vide (retourne True) ou non (retourne False)"""
        if len(self.__stack) != 0 :
            return False
        else :
            return True

########################################################################################################################################################
###ACTIVITE 2 - APPLICATIONS############################################################################################################################
########################################################################################################################################################

##A FAIRE Q3 à Q5 ##

def bien_parenthese(ch):
    pass   
    
    
    
    
    
    
    
    
    
    
    
    


##Pour vérifier, activez les lignes ci-dessous :

#print("Activité 2 - Q4")
#ch1 = "(5x3)+7"
#ch2 = "(5x3)+6)"
#print(ch1," est bien parenthésée : ",bien_parenthese(ch1))
#print(ch2," est bien parenthésée : ",bien_parenthese(ch2))

##A FAIRE Q5##

def bien_parenthesebis(ch):
    pass


    
    
    
    
    
    
    
    
    
    
    

##Pour vérifier, activez les lignes ci-dessous :

#print("Activité 2 - Q5")
#ch1 = "(5x3)+[7x6]"
#ch2 = "(5x3)+[6x9)"
#print(ch1," est bien parenthésée : ",bien_parenthese(ch1))
#print(ch2," est bien parenthésée : ",bien_parenthese(ch2))


##A FAIRE Q6 et Q8##


def stack(n):#création d'une pile de longueur n
    pass



def push(P,x) : #on insère la donnée x au sommet de la pile si celle-ci n'est pas pleine
    pass



def pop(P) : #on retire la dernière donnée de la pile si elle n'est pas vide et on la retourne
    pass



def is_full(P) : #on teste si la pile est pleine ou pas
    pass



def is_empty(P) : #on teste si la pile est vide ou pas
    pass



def top_peek(P) : #on retourne sans l'enlever la donnée en haut de la pile
    pass




##Pour vérifier, activez les lignes ci-dessous :
    
#P = stack(4)
#print(P)
#push(P,3)
#push(P,5)
#push(P,8)
#N=pop(P)
#push(P,11)
#push(P,13)
#print("Pile P :",P,", N = ",N)
#print("is_empty(P) renvoie ",is_empty(P))
#print("is_full(P) renvoie ",is_full(P))
#print("top_peek() donne ",top_peek(P))


#A FAIRE Q10##

def inverse_stack(P):#prend une pile en paramètre et retourne cette pile à l'envers
    pass



def calculatrice_polonaise(P):
    pass



#Pour vérifier, activez les lignes ci-dessous :

#P = [7,2,"+",24,"*"]
#print("24*(7+2) = ",calculatrice_polonaise(P))
                        
########################################################################################################################################################
########################################################################################################################################################
########################################################################################################################################################

