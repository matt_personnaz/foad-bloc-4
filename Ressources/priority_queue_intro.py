# -*- coding: utf-8 -*-
"""
Created on Sat Jun 13 18:04:32 2020

@author: Matt
"""


customers = []
customers.append((1, "Rémi")) #no sort needed here because 1 item. 
customers.append((1, "Rayane"))
customers.sort(reverse=True)  #Need to sort to maintain order
customers.append((5, "Léon"))
customers.sort(reverse=True) 
#Need to sort to maintain order
customers.append((3, "Delphine"))
customers.sort(reverse=True)
while customers:
     print(customers.pop(0)) 
     #Will print names in the order: Léon, Delphine, Rémi, Rayane. 