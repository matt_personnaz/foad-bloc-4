# This program traverses a maze using a queue.

from sq import Queue              

MAZE_SIZE = 10  # size of the maze
		
#trace the given maze
def print_maze(maze):   
	for row in range(MAZE_SIZE):
		print(maze[row])
	print
		
# Return true if the given (x,y) point is in the maze
def in_bounds(xx,yy):              
	return (0 <= xx < MAZE_SIZE) and (0 <= yy < MAZE_SIZE)

	
 # traverse 'maze' from starting coordinates 'start'
def solve_maze_queue(maze, start):         
	q = Queue()                  
	q.enqueue(start);            # enqueue the start coordinates into q
	while not q.is_empty():       # loop while q is not empty
		(x, y) = q.dequeue()      # dequeue a coordinate from q into the tuple (x,y)
		if in_bounds(x,y):         
			if maze[x][y] == 'G':    # if (x,y) is the goal then
				q.clear()             # empty the queue because we're done
			elif maze[x][y] == ' ':  # else if (x,y) is an undiscovered coordinate
				maze[x] = maze[x][:y] + 'D' + maze[x][y+1:]  # mark (x,y) discovered with 'D'
				print_maze(maze);      # print the maze to show progress so far
				q.enqueue((x+1, y))   # enqueue right neighbor into queue q
				q.enqueue((x-1, y))   # enqueue left neighbor into queue q
				q.enqueue((x, y-1))   # enqueue upper neighbor into queue q
				q.enqueue((x, y+1))   # enqueue lower neighbor into queue q
				
# The following can be used to create a maze and traverse it:

import sys
input = open('maze.dat', 'r')     # open the file 'maze.dat' for reading
maze = input.readlines();         # read the file into maze
solve_maze_queue(maze, (0,0))                 # traverse the maze starting at (0,0)
