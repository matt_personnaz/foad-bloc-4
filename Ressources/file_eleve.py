# -*- coding: utf-8 -*-
"""
Created on Fri Jun 08 09:48:54 2020

@author: Alexandre Dicharry
"""

#########################################################################################################################################################
###Partie 2 : LES FILES ####################################################################################################################
#########################################################################################################################################################

##A FAIRE Q1 ##

class File :
    """Classe File"""
    
    def __init__(self):
        """Constructeur"""
        self.file = []
        
    def __str__(self):
        """Afficheur"""
        s = 'File = '
        for e in self.file :
            s = s + str(e) + '->'
        return s

    def enfiler(self,e):
        """enfile l'élément e dans la file"""
    
    def defiler(self):
        """Supprime le premier élément de la file"""

    def traiter(self):
        """Retourne et supprime le premier élément de la file"""
    
    def sommet(self):
        """Retourne le premier élément de la file"""
    
    def est_vide(self):
        """Test si la file est vide ou non"""

##Pour vérifier, activez les lignes ci-dessous puis exécuter chaque méthode.
#F = File()
#F.enfiler(2)
#F.enfiler(4)
#F.enfiler(6)
#F.enfiler(8)

##A FAIRE Q2 ##
    
#La classe pile de la partie I est donnée ci-dessous :

class Pile :
    """Classe Pile"""
    
    def __init__(self) :
        self.pile = []
        
    def __str__(self) :
        s = 'Pile : '
        for e in self.pile :
            s = s + str(e) + '/'
        return s
    
    def empiler(self,e):
        self.pile.insert(0,e)
    
    def depiler(self):
        del self.pile[0]
    
    def traiter(self):
        i = self.pile[0]
        del self.pile[0]
        return i
    
    def sommet(self):
        return self.pile[0]
    
    def est_vide(self):
        return len(self.pile) == 0

#Implémenter la fonction inverse_file() :

def inverse_file(F):
    
    

    
    
    
    
    
    
    

#Pour vérifier, activez les lignes ci-dessous puis exécuter chaque méthode.
#F = File()
#F.enfiler(2)
#F.enfiler(4)
#F.enfiler(6)
#F.enfiler(8)
#print('La file : ',F.__str__())
#print('Son contraire ',inverse_file(F))





